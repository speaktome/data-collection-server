package com.uom.speaktome.common;

import java.io.DataInputStream;
import java.io.IOException;

import org.LiveGraph.LiveGraph;
import org.LiveGraph.dataFile.write.DataStreamWriter;
import org.LiveGraph.dataFile.write.DataStreamWriterFactory;
import org.LiveGraph.settings.DataFileSettings;

public class DataCollectionService implements Runnable {
	private static final boolean ADAPTIVE_ACCEL_FILTER = true;
	float lastAccel[] = new float[3];
	float accelFilter[] = new float[3];

	public static final String DEMO_DIR = System.getProperty("user.dir");
	public static final int MIN_SLEEP = 0;
	public static final int MAX_SLEEP = 1000;
	public static final int MIN_BURST = 1;
	public static final int MAX_BURST = 10;
	public static int MAX_DATASETS = 50;

	public boolean running = true;

	private String file_name = "default";
	private boolean isDisplayGraphs = true;
	private LiveGraph app = null;

	private DataInputStream din = null;
	private DataStreamWriter dout = null;
	/*
	 * Filter parameters
	 */
	float updateFreq = 30;
	float cutOffFreq = 0.9f;
	float kAccelerometerMinStep = 0.033f;
	float kAccelerometerNoiseAttenuation = 3.0f;

	public DataCollectionService(DataInputStream din, Parameters parameters) {
		this.din = din;
		this.file_name = parameters.getFile_name();
		this.isDisplayGraphs = parameters.isDisplayGraphs();
		this.MAX_DATASETS = parameters.getMAX_DATASETS();
		this.updateFreq = parameters.getUpdateFreq();
		this.cutOffFreq = parameters.getCutOffFreq();
		this.kAccelerometerMinStep = parameters.getkAccelerometerMinStep();
		this.kAccelerometerNoiseAttenuation = parameters
				.getkAccelerometerNoiseAttenuation();

	}

	public void terminamte() {
		this.running = false;
		dout = null;
		if (app != null) {
			app.disposeGUIAndExit();
			app = null;
		}
	}

	public float[] onAccelerometerChanged(float accelX, float accelY,
			float accelZ) {// high pass filter

		float RC = 1.0f / cutOffFreq;
		float dt = 1.0f / updateFreq;
		float filterConstant = RC / (dt + RC);
		float alpha = filterConstant;

		if (ADAPTIVE_ACCEL_FILTER) {
			float d = clamp(
					Math.abs(norm(accelFilter[0], accelFilter[1],
							accelFilter[2]) - norm(accelX, accelY, accelZ))
							/ kAccelerometerMinStep - 1.0f, 0.0f, 1.0f);
			alpha = d * filterConstant / kAccelerometerNoiseAttenuation
					+ (1.0f - d) * filterConstant;
		}

		accelFilter[0] = (float) (alpha * (accelFilter[0] + accelX - lastAccel[0]));
		accelFilter[1] = (float) (alpha * (accelFilter[1] + accelY - lastAccel[1]));
		accelFilter[2] = (float) (alpha * (accelFilter[2] + accelZ - lastAccel[2]));
		return accelFilter;
	}

	public static <T extends Comparable<T>> T clamp(T val, T min, T max) {
		if (val.compareTo(min) < 0)
			return min;
		else if (val.compareTo(max) > 0)
			return max;
		else
			return val;
	}

	private float norm(float accelX, float accelY, float accelZ) {
		return (float) Math.sqrt(accelX * accelX + accelY * accelY + accelZ
				* accelZ);
	}

	@Override
	public void run() {
		dout = DataStreamWriterFactory.createDataWriter(DEMO_DIR, file_name);
		dout.setSeparator(",");

		dout.addDataSeries("X");
		dout.addDataSeries("Y");
		dout.addDataSeries("Z");

		DataFileSettings dfs = new DataFileSettings();
		dfs.setDataFile(DEMO_DIR + "\\" + file_name + ".lgdat");
		dfs.setUpdateFrequency(10);
		dfs.save(DEMO_DIR + "\\startup.lgdfs");

		int k = 0;

		int datasetNumber = 0;

		if (isDisplayGraphs) {
			app = LiveGraph.application();
			app.exec(new String[] { "-dfs", DEMO_DIR + "\\startup.lgdfs" });
		}

		long startMillis = System.currentTimeMillis();

		byte[] bytes1 = new byte[15];

		try {
			while (((k = din.read(bytes1, 0, 12)) > 0)
					&& MAX_DATASETS > datasetNumber && running) {

				int x = (bytes1[0] & 0xFF) << 24 | ((bytes1[1] & 0xFF) << 16)
						| ((bytes1[2] & 0xFF) << 8) | ((bytes1[3] & 0xFF));

				int y = (bytes1[4] & 0xFF) << 24 | ((bytes1[5] & 0xFF) << 16)
						| ((bytes1[6] & 0xFF) << 8) | ((bytes1[7] & 0xFF));
				int z = (bytes1[8] & 0xFF) << 24 | ((bytes1[9] & 0xFF) << 16)
						| ((bytes1[10] & 0xFF) << 8) | ((bytes1[11] & 0xFF));

				float asFloatX = Float.intBitsToFloat(x);
				float asFloatY = Float.intBitsToFloat(y);
				float asFloatZ = Float.intBitsToFloat(z);

				onAccelerometerChanged(asFloatX, asFloatY, asFloatZ);
				// Set-up the data values:
				// out.setDataValue(System.currentTimeMillis() -
				// startMillis);
				dout.setDataValue(accelFilter[0]);
				dout.setDataValue(accelFilter[1]);
				dout.setDataValue(accelFilter[2]);

				// Write dataset to disk:
				dout.writeDataSet();
				// Check for IOErrors:
				if (dout.hadIOException()) {
					dout.getIOException().printStackTrace();
					dout.resetIOException();
				}
				datasetNumber++;
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		Thread.yield();
		long sleep = (long) (MIN_SLEEP + (Math.random() * (double) (MAX_SLEEP - MIN_SLEEP)));
		try {
			Thread.sleep(sleep);
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}
		Thread.yield();
		dout.close();
		System.out.println(file_name + " Data collection finished.");

	}
}
