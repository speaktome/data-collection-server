package com.uom.speaktome.common;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.IOException;

import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;

public class Launcher {

	private JFrame frmSpeaktomeServer;
	private JTextField txtFieldUUID;
	private JTextField txtFieldTag;
	private JTextField txtFieldNoFeatures;
	private JTextField txtFieldUpdateFre;
	private JTextField txtFieldCutOff;
	private JTextField txtFieldAccStep;
	private JTextField txtFieldAccNoiseAtt;
	private JTextField txtFeildTimeStamp;
	private JTextField txtFieldReadFre;
	private JTextPane txtPaneConsole;
	private StyledDocument styledDocument;
	private JCheckBox chckbxDisplayGraphs;
	private JButton btnStart;

	private StreamConnectionNotifier server = null;
	private DataInputStream din = null;
	private DataCollectionService dataCollectionService;
	private Thread dataCollectionThread = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Launcher window = new Launcher();
					window.frmSpeaktomeServer.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Launcher() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSpeaktomeServer = new JFrame();
		frmSpeaktomeServer.setTitle("SpeakToMe Server\r\n");
		frmSpeaktomeServer.setBounds(100, 100, 539, 453);
		frmSpeaktomeServer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSpeaktomeServer.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Server", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		panel.setBounds(252, 291, 261, 113);
		frmSpeaktomeServer.getContentPane().add(panel);
		panel.setLayout(null);

		btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				performStartAction(e);
			}

		});
		btnStart.setBounds(169, 22, 73, 23);
		panel.add(btnStart);

		JButton btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (server != null) {
					try {
						server.close();
					} catch (IOException e1) {
						try {
							styledDocument.insertString(styledDocument.getLength(),
									"Bluetooth server shutdown complete.\n", null);
						} catch (BadLocationException e) {
						}
					}
				}
				if (dataCollectionService != null) {
					dataCollectionService.terminamte();
					dataCollectionService = null;
				}
				if (dataCollectionThread != null) {
					dataCollectionThread.stop();
					dataCollectionThread = null;
				}

				try {
					styledDocument.insertString(styledDocument.getLength(),
							"Bluetooth server shutdown complete.\n", null);
				} catch (BadLocationException e) {

				}
			}
		});
		btnStop.setBounds(169, 51, 73, 23);
		panel.add(btnStop);

		txtFieldUUID = new JTextField();
		txtFieldUUID.setBounds(10, 52, 154, 20);
		panel.add(txtFieldUUID);
		txtFieldUUID.setColumns(10);

		JLabel lblUuid = new JLabel("UUID");
		lblUuid.setBounds(10, 26, 46, 14);
		panel.add(lblUuid);

		chckbxDisplayGraphs = new JCheckBox("Display graphs");
		chckbxDisplayGraphs.setBounds(10, 79, 154, 23);
		panel.add(chckbxDisplayGraphs);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Noise", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		panel_2.setBounds(23, 11, 211, 155);
		frmSpeaktomeServer.getContentPane().add(panel_2);
		panel_2.setLayout(null);

		txtFieldUpdateFre = new JTextField();
		txtFieldUpdateFre.setColumns(10);
		txtFieldUpdateFre.setBounds(125, 28, 76, 20);
		panel_2.add(txtFieldUpdateFre);

		txtFieldCutOff = new JTextField();
		txtFieldCutOff.setColumns(10);
		txtFieldCutOff.setBounds(125, 59, 76, 20);
		panel_2.add(txtFieldCutOff);

		txtFieldAccStep = new JTextField();
		txtFieldAccStep.setColumns(10);
		txtFieldAccStep.setBounds(125, 90, 76, 20);
		panel_2.add(txtFieldAccStep);

		JLabel lblUpdateFre = new JLabel("Update Fre");
		lblUpdateFre.setBounds(20, 31, 65, 14);
		panel_2.add(lblUpdateFre);

		JLabel lblCutoffFre = new JLabel("Cut-off Fre");
		lblCutoffFre.setBounds(20, 62, 65, 14);
		panel_2.add(lblCutoffFre);

		JLabel lblAccMinStep = new JLabel("Acc Min Step");
		lblAccMinStep.setBounds(20, 93, 76, 14);
		panel_2.add(lblAccMinStep);

		txtFieldAccNoiseAtt = new JTextField();
		txtFieldAccNoiseAtt.setColumns(10);
		txtFieldAccNoiseAtt.setBounds(125, 118, 76, 20);
		panel_2.add(txtFieldAccNoiseAtt);

		JLabel lblAccMinStep_1 = new JLabel("Acc noise att.");
		lblAccMinStep_1.setBounds(20, 121, 76, 14);
		panel_2.add(lblAccMinStep_1);

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Store",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setBounds(23, 177, 211, 191);
		frmSpeaktomeServer.getContentPane().add(panel_3);
		panel_3.setLayout(null);

		JLabel lblTag = new JLabel("Tag");
		lblTag.setBounds(10, 24, 25, 14);
		panel_3.add(lblTag);

		txtFieldTag = new JTextField();
		txtFieldTag.setBounds(85, 21, 115, 20);
		panel_3.add(txtFieldTag);
		txtFieldTag.setColumns(10);

		JLabel lblTimeStamp = new JLabel("No of fetures");
		lblTimeStamp.setBounds(10, 49, 65, 14);
		panel_3.add(lblTimeStamp);

		txtFieldNoFeatures = new JTextField();
		txtFieldNoFeatures.setColumns(10);
		txtFieldNoFeatures.setBounds(85, 46, 115, 20);
		panel_3.add(txtFieldNoFeatures);

		JLabel lblReadFre = new JLabel("Read Fre.");
		lblReadFre.setBounds(10, 75, 55, 14);
		panel_3.add(lblReadFre);

		JLabel label_1 = new JLabel("Time Stamp");
		label_1.setBounds(10, 106, 55, 14);
		panel_3.add(label_1);

		txtFieldReadFre = new JTextField();
		txtFieldReadFre.setColumns(10);
		txtFieldReadFre.setBounds(85, 77, 115, 20);
		panel_3.add(txtFieldReadFre);

		txtFeildTimeStamp = new JTextField();
		txtFeildTimeStamp.setBounds(85, 103, 115, 20);
		panel_3.add(txtFeildTimeStamp);
		txtFeildTimeStamp.setColumns(10);

		JButton btnNewButton = new JButton("Record");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (din != null) {
					startRecord();
				} else {
					JOptionPane.showMessageDialog(frmSpeaktomeServer,
							"Please check start the server!");
				}

			}
		});
		btnNewButton.setBounds(111, 145, 89, 23);
		panel_3.add(btnNewButton);

		JButton btnCloseGraph = new JButton("Close Graph");
		btnCloseGraph.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (dataCollectionService != null)
					dataCollectionService.terminamte();
				if (dataCollectionThread != null)
					dataCollectionThread.destroy();
				dataCollectionThread = null;
			}
		});
		btnCloseGraph.setBounds(10, 145, 91, 23);
		panel_3.add(btnCloseGraph);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Console",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(254, 11, 220, 271);
		frmSpeaktomeServer.getContentPane().add(panel_1);
		panel_1.setLayout(null);

		txtPaneConsole = new JTextPane();
		styledDocument = txtPaneConsole.getStyledDocument();
		txtPaneConsole.setBounds(10, 21, 200, 239);
		panel_1.add(txtPaneConsole);

		initDefaultParameters();
	}

	protected void startRecord() {
		if (dataCollectionService != null) {
			dataCollectionService.terminamte();
			dataCollectionService = null;
		}
		if (dataCollectionThread != null) {
			dataCollectionThread.stop();
			dataCollectionThread = null;
		}
		try {
			int r = 0;
			if ((r = din.available()) > 0) {
				byte b[] = new byte[r];
				din.readFully(b);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		float updateFreq = 30;
		float cutOffFreq = 0.9f;
		float kAccelerometerMinStep = 0.033f;
		float kAccelerometerNoiseAttenuation = 3.0f;
		try {
			updateFreq = Integer.parseInt(txtFieldUpdateFre.getText());
			cutOffFreq = Float.parseFloat(txtFieldCutOff.getText());
			kAccelerometerMinStep = Float.parseFloat(txtFieldAccStep.getText());
			kAccelerometerNoiseAttenuation = Float
					.parseFloat(txtFieldAccNoiseAtt.getText());
		} catch (Exception exp) {
			JOptionPane.showMessageDialog(frmSpeaktomeServer,
					"Please check the parameters before proceed!");
			return;
		}

		int mAX_DATASETS = Integer.parseInt(txtFieldNoFeatures.getText());

		String UUID = txtFieldUUID.getText();
		String file_name = txtFieldTag.getText();

		boolean isDisplayGraphs = chckbxDisplayGraphs.isSelected();

		Parameters parameters = new Parameters();
		parameters.setCutOffFreq(cutOffFreq);
		parameters.setDisplayGraphs(isDisplayGraphs);
		parameters.setFile_name(file_name);
		parameters.setkAccelerometerMinStep(kAccelerometerMinStep);
		parameters.setMAX_DATASETS(mAX_DATASETS);

		dataCollectionService = new DataCollectionService(din, parameters);
		Thread r = new Thread(dataCollectionService);
		r.start();
	}

	protected void performStartAction(ActionEvent e) {
		if (dataCollectionService != null) {
			dataCollectionService.terminamte();
			dataCollectionService = null;
		}

		try {
			String name = "Echo Server"; // the name of the service
			String url = "btspp://localhost:" + txtFieldUUID.getText()
					+ ";name=" + name + ";authenticate=false;encrypt=false;";
			LocalDevice local = null;
			StreamConnection conn = null;

			styledDocument.insertString(styledDocument.getLength(),
					"Setting device to be discoverable...\n", null);

			local = LocalDevice.getLocalDevice();
			local.setDiscoverable(DiscoveryAgent.GIAC);
			styledDocument.insertString(styledDocument.getLength(),
					"Start advertising service...\n", null);
			server = (StreamConnectionNotifier) Connector.open(url);
			styledDocument.insertString(styledDocument.getLength(),
					"Waiting for incoming connection...\n", null);
		
			conn = server.acceptAndOpen();
			styledDocument.insertString(styledDocument.getLength(),
					"Client Connected...\n", null);
			din = new DataInputStream(conn.openInputStream());
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(frmSpeaktomeServer,
					"Please check the Bluetooth connection!");
		}
	}

	private void initDefaultParameters() {
		txtFieldUpdateFre.setText("30");
		txtFieldCutOff.setText("0.9f");
		txtFieldAccStep.setText("0.033f");
		txtFieldAccNoiseAtt.setText("3.0f");
		txtFieldNoFeatures.setText("50");
		txtFieldReadFre.setText("10");
		txtFieldUUID.setText("0000110100001000800000805F9B34FB");
		txtFieldTag.setText("lift");
		chckbxDisplayGraphs.setSelected(true);
	}
}
