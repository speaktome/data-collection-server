package com.uom.speaktome.common;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Scanner;

import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;

import org.LiveGraph.LiveGraph;
import org.LiveGraph.dataFile.write.DataStreamWriter;
import org.LiveGraph.dataFile.write.DataStreamWriterFactory;
import org.LiveGraph.settings.DataFileSettings;

public class LiveGraphDemo {

	private static final boolean ADAPTIVE_ACCEL_FILTER = true;
	float lastAccel[] = new float[3];
	float accelFilter[] = new float[3];

	public static final String DEMO_DIR = System.getProperty("user.dir");

	public static final int MIN_SLEEP = 0;
	public static final int MAX_SLEEP = 1000;
	public static final int MIN_BURST = 1;
	public static final int MAX_BURST = 10;
	public static final int MAX_DATASETS = 50;

	public final UUID uuid = new UUID("0000110100001000800000805F9B34FB", false);
	public final String name = "Echo Server"; // the name of the service
	public final String url = "btspp://localhost:" + uuid // the service url
			+ ";name=" + name + ";authenticate=false;encrypt=false;";
	LocalDevice local = null;
	StreamConnectionNotifier server = null;
	StreamConnection conn = null;

	public void exec() throws IOException {

		System.out.println("Enter the file name");
		Scanner s = new Scanner(System.in);

		String file_name = s.nextLine();

		DataStreamWriter out = DataStreamWriterFactory.createDataWriter(
				DEMO_DIR, file_name);
		out.setSeparator(",");
		out.writeFileInfo("LiveGraph demo file : " + file_name);

		out.addDataSeries("X");
		out.addDataSeries("Y");
		out.addDataSeries("Z");

		DataFileSettings dfs = new DataFileSettings();
		dfs.setDataFile(DEMO_DIR + "\\" + file_name + ".lgdat");
		dfs.setUpdateFrequency(10);
		dfs.save(DEMO_DIR + "\\startup.lgdfs");

		System.out.println("Setting device to be discoverable...");
		local = LocalDevice.getLocalDevice();
		local.setDiscoverable(DiscoveryAgent.GIAC);
		System.out.println("Start advertising service...");
		server = (StreamConnectionNotifier) Connector.open(url);
		System.out.println("Waiting for incoming connection...");
		conn = server.acceptAndOpen();
		System.out.println("Client Connected...");
		DataInputStream din = new DataInputStream(conn.openInputStream());
		int k = 0;

		int datasetNumber = 0;

		LiveGraph app = LiveGraph.application();
		app.exec(new String[] { "-dfs", DEMO_DIR + "\\startup.lgdfs" });
		long startMillis = System.currentTimeMillis();

		while (MAX_DATASETS > datasetNumber) {

			byte[] bytes1 = new byte[15];

			while (((k = din.read(bytes1, 0, 12)) > 0)
					&& MAX_DATASETS > datasetNumber) {

				int x = (bytes1[0] & 0xFF) << 24 | ((bytes1[1] & 0xFF) << 16)
						| ((bytes1[2] & 0xFF) << 8) | ((bytes1[3] & 0xFF));

				int y = (bytes1[4] & 0xFF) << 24 | ((bytes1[5] & 0xFF) << 16)
						| ((bytes1[6] & 0xFF) << 8) | ((bytes1[7] & 0xFF));
				int z = (bytes1[8] & 0xFF) << 24 | ((bytes1[9] & 0xFF) << 16)
						| ((bytes1[10] & 0xFF) << 8) | ((bytes1[11] & 0xFF));

				float asFloatX = Float.intBitsToFloat(x);
				float asFloatY = Float.intBitsToFloat(y);
				float asFloatZ = Float.intBitsToFloat(z);

				onAccelerometerChanged(asFloatX, asFloatY, asFloatZ);
				// Set-up the data values:
				// out.setDataValue(System.currentTimeMillis() - startMillis);
				out.setDataValue(accelFilter[0]);
				out.setDataValue(accelFilter[1]);
				out.setDataValue(accelFilter[2]);

				// Write dataset to disk:
				out.writeDataSet();
				// Check for IOErrors:
				if (out.hadIOException()) {
					out.getIOException().printStackTrace();
					out.resetIOException();
				}
				datasetNumber++;
			}

			Thread.yield();
			long sleep = (long) (MIN_SLEEP + (Math.random() * (double) (MAX_SLEEP - MIN_SLEEP)));
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
			Thread.yield();
		}
		out.close();
		System.out.println("Demo finished. Cheers.");
	}

	public float[] onAccelerometerChanged(float accelX, float accelY,
			float accelZ) {
		// high pass filter
		float updateFreq = 30; // match this to your update speed
		float cutOffFreq = 0.9f;
		float RC = 1.0f / cutOffFreq;
		float dt = 1.0f / updateFreq;
		float filterConstant = RC / (dt + RC);
		float alpha = filterConstant;
		float kAccelerometerMinStep = 0.033f;
		float kAccelerometerNoiseAttenuation = 3.0f;

		if (ADAPTIVE_ACCEL_FILTER) {
			float d = clamp(
					Math.abs(norm(accelFilter[0], accelFilter[1],
							accelFilter[2]) - norm(accelX, accelY, accelZ))
							/ kAccelerometerMinStep - 1.0f, 0.0f, 1.0f);
			alpha = d * filterConstant / kAccelerometerNoiseAttenuation
					+ (1.0f - d) * filterConstant;
		}

		accelFilter[0] = (float) (alpha * (accelFilter[0] + accelX - lastAccel[0]));
		accelFilter[1] = (float) (alpha * (accelFilter[1] + accelY - lastAccel[1]));
		accelFilter[2] = (float) (alpha * (accelFilter[2] + accelZ - lastAccel[2]));
		return accelFilter;
	}

	public static <T extends Comparable<T>> T clamp(T val, T min, T max) {
		if (val.compareTo(min) < 0)
			return min;
		else if (val.compareTo(max) > 0)
			return max;
		else
			return val;
	}

	private float norm(float accelX, float accelY, float accelZ) {
		return (float) Math.sqrt(accelX * accelX + accelY * accelY + accelZ
				* accelZ);
	}

	public static void main(String[] unusedArgs) throws IOException {
		(new LiveGraphDemo()).exec();
	}

}