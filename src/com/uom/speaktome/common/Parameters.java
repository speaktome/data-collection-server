package com.uom.speaktome.common;

public class Parameters {
	private float updateFreq = 30;
	private float cutOffFreq = 0.9f;
	private float kAccelerometerMinStep = 0.033f;
	private float kAccelerometerNoiseAttenuation = 3.0f;
	private String file_name = "deafult";
	private boolean isDisplayGraphs = true;
	private int MAX_DATASETS = 50;

	public int getMAX_DATASETS() {
		return this.MAX_DATASETS;
	}

	public void setMAX_DATASETS(int mAX_DATASETS) {
		this.MAX_DATASETS = mAX_DATASETS;
	}

	public boolean isDisplayGraphs() {
		return this.isDisplayGraphs;
	}

	public void setDisplayGraphs(boolean isDisplayGraphs) {
		this.isDisplayGraphs = isDisplayGraphs;
	}

	public String getFile_name() {
		return this.file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public float getUpdateFreq() {
		return this.updateFreq;
	}

	public void setUpdateFreq(float updateFreq) {
		this.updateFreq = updateFreq;
	}

	public float getCutOffFreq() {
		return this.cutOffFreq;
	}

	public void setCutOffFreq(float cutOffFreq) {
		this.cutOffFreq = cutOffFreq;
	}

	public float getkAccelerometerMinStep() {
		return this.kAccelerometerMinStep;
	}

	public void setkAccelerometerMinStep(float kAccelerometerMinStep) {
		this.kAccelerometerMinStep = kAccelerometerMinStep;
	}

	public float getkAccelerometerNoiseAttenuation() {
		return this.kAccelerometerNoiseAttenuation;
	}

	public void setkAccelerometerNoiseAttenuation(
			float kAccelerometerNoiseAttenuation) {
		this.kAccelerometerNoiseAttenuation = kAccelerometerNoiseAttenuation;
	}

}
